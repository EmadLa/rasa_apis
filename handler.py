import yaml


# from rasa.model import get_local_model
# from rasa.core.agent import load_agent


class Handler:

    def openFile(self, path, mode):
        with open(path, mode) as file:
            content = yaml.safe_load(file)
        return content

    # def add_intent_to_nlu_file(self, intent, examples):
    #     nlu = self.openFile('data/nlu.yml', 'r')
    #     if nlu is not None:
    #         nlu["nlu"].append(
    #             {"intent": intent, "examples": yaml.dump([examples[i] for i in range(len(examples))], indent=4)})
    #         with open('data/nlu.yml', 'w') as file:
    #             yaml.dump(nlu, file)
    #             self.add_intent_to_domain_file(intent)
    #     else:
    #         print("Failed to load nlu file")
    #
    # def add_intent_to_domain_file(self, intent_name):
    #     with open('domain.yml', 'r') as file:
    #         domain = yaml.safe_load(file)
    #     if domain is not None:
    #         domain['intents'].append(intent_name)
    #
    #         with open('domain.yml', 'w') as file:
    #             yaml.dump(domain, file)
    #     else:
    #         print("Failed to load domain file")
    #
    # async def load_rasa_agent(self, model_path):
    #     agent = await load_agent(model_path)
    #     return agent
    #
    # def get_model(self, name):
    #     model_name = name + ".tar.gz"
    #     model_path = get_local_model("models/" + model_name)
    #     model = self.load_rasa_agent(model_path)
    #     print(model_path)
    #     if model_path is not None:
    #         print("Found model at path: ", model_path)
    #     else:
    #         print("Could not find model with name: ", model_name)
    #
    # def get_intents(self):
    #     nlu = self.openFile("domain.yml", 'r')
    #     print(nlu['intents'])
    #
    # def get_examples_to_intent(self, intent):
    #     nlu = self.openFile("data/nlu.yml", 'r')
    #     nlu_content = nlu['nlu']
    #     for i in range(len(nlu_content)):
    #         if nlu_content[i]['intent'] == intent:
    #             print(nlu_content[i]['examples'])
    #             return nlu_content[i]['examples']
    #     return ""

    def add_response(self, intent, responses):
        # response = self.openFile("domain.yml", 'r')
        # print(responses)
        # if response is not None:
        #
        #     # response.append({"responses": "utter_" + intent, [key : yml.dump(value) for key,value in responses]})
        #     #             {"intent": intent, "examples": yaml.dump([examples[i] for i in range(len(examples))], indent=4)})
        #
        #     with open('domain.yml', 'w') as file:
        #         yaml.dump(response, file)
        # else:
        #     print("No responses")
        new_response = {"utter_greet": ["Hello, how can I help you today?"]}

        # Load the existing domain file
        with open("domain.yml", "r") as f:
            domain = yaml.safe_load(f)

        # Add the new response to the domain file
        domain["responses"].update(new_response)

        # Write the updated domain file back out to the file
        with open("domain.yml", "w") as f:
            yaml.safe_dump(domain, f)